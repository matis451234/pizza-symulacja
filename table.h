#ifndef TABLE_H
#define TABLE_H

#include <iostream>
#include <string>
#include <vector>
#include "pizza_client.h"

using namespace std;

class Table_Inter
{
public:
    virtual ~Table_Inter();
    virtual int getID() = 0;
    virtual int getSize() = 0;
    virtual int emptySeats()= 0 ; // get emptySeats
    virtual bool is_empty() = 0;
    virtual bool canSitDown(ClientGroup *possible_client) = 0;
    virtual bool can_sit_empty(ClientGroup *possible_client)= 0;
    virtual void addClientGroup(ClientGroup *new_client) = 0;
    virtual bool removeClientGroup(ClientGroup *old_client)= 0;
};

class Table: public Table_Inter
{
protected:
    int ID;
    int size;
    int empty;
    vector<ClientGroup*> clients;
public:
    Table(const int ID,const  int size);
    Table();
    int getID();
    int getSize();
    int emptySeats(); // get emptySeats
    bool is_empty();
    bool canSitDown(ClientGroup *possible_client);
    bool can_sit_empty(ClientGroup *possible_client);
    void addClientGroup(ClientGroup *new_client);
    bool removeClientGroup(ClientGroup *old_client);
    friend ostream& operator<<(ostream& os, const Table& obj);
};

#endif