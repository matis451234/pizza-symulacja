#include <vector>
#include <iostream>
#include <sstream>
#include "products.h"
#include "extras.h"

using namespace std;
#ifndef BILL_H
#define BILL_H
class Bill_Inter
{
public:
    virtual ~Bill_Inter(){};
    virtual void add_position(string, int, int, int) = 0;
    virtual void print_bill(Clock) = 0;
    virtual void remove_position(int) = 0;
    virtual void add_discount(int) = 0;
    virtual void remove_discount() = 0;
    virtual int get_sum() = 0;
    virtual int get_time_prepare() = 0;
    virtual string get_printed_bill() = 0;
    virtual string get_simple_bill() = 0;
};

class Bill: public Bill_Inter
{

    string  seller;
    vector <string> list_of_names;
    vector <int> list_of_count;
    vector <int> list_of_prices;
    vector <int> list_of_times;
    stringstream ss;


    int sum;
    int time_prepare;
    int discount = 0;

    public:
    Bill(int = 0);
    virtual void add_position(string, int, int, int);
    virtual void print_bill(Clock);
    virtual void remove_position(int);
    virtual void add_discount(int);
    virtual void remove_discount();
    virtual int get_sum();
    virtual int get_time_prepare();
    virtual string get_printed_bill();
    virtual string get_simple_bill();
};

#endif

