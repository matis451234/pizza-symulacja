#include "simulation.h"

#include <vector>
#include <list>
#include <queue>
#include <string>
#include <memory>
#include <random>
#include <chrono>
#include <exception>

using namespace std;


Simulation::Simulation(int sim_whole_time, int client_density, int num_of_waiters, vector<Table> table_list,
                       vector<Pizza> pizza_list, vector<Drink> drink_list, vector<Snack> snack_list,
                       int start_hour, int start_minute){
    this->sim_whole_time = sim_whole_time;
    this->client_density = client_density;
    this->sim_time = Clock(start_hour, start_minute);
    this->num_of_waiters = num_of_waiters;
    this->funds = 0;
    this->table_list = table_list;
    this->num_of_tables = table_list.size();
    this->pizza_list = pizza_list;
    this->drink_list = drink_list;
    this->snack_list = snack_list;
    this->skipping_on = 0;

    generate_waiter_list();
    unsigned seed = chrono::system_clock::now().time_since_epoch().count();
    mt19937 gn (seed);
    this->gn = gn;
}


void Simulation::generate_waiter_list(){
    this->waiter_list.reserve(num_of_waiters);
    for(int i=0; i<num_of_waiters; i++){
        waiter_list.push_back(Waiter(i));
    }
}


void Simulation::spawn_clients(){
    bernoulli_distribution dis_1(client_density/60.0);
    bool to_spawn = dis_1(gn);
    if(to_spawn){
        poisson_distribution<int> dis_2(2);
        int num_of_clients = dis_2(gn) + 1;
        Client g_leader;
        ClientGroup group ({g_leader}, g_leader);
        client_group_list.push_back(group);
        ClientGroup* group_ptr = &client_group_list.back();
        if(num_of_clients > 1){
            for(int i=1; i<num_of_clients; i++){
                group_ptr->add_client(Client());
            }
        }
        sim_out << "group nr " << group_ptr->get_id() << " of " << group_ptr->get_num_of_clients() << " clients arrived, ";
        normal_distribution<double> dis_3(3000, 1000);
        int budget = min(800, int(dis_3(gn)));
        group_ptr->set_budget(budget);

        bool is_seated = 0;

        //set table
        //1. empty tables
        for(int i=0; i<num_of_tables; i++){
            if(table_list[i].can_sit_empty(group_ptr)){
                table_list[i].addClientGroup(group_ptr);  //teraz grupa nie zna stolika, ale stolik zna ja
                sim_out << "seated alone at table " << table_list[i].getID() << endl;
                is_seated = 1;
                break;
            }
        }

        //2. joining
        if(!is_seated){
            for(int i=0; i<num_of_tables; i++){
                if(table_list[i].canSitDown(group_ptr)){
                    table_list[i].addClientGroup(group_ptr);  //teraz grupa nie zna stolika, ale stolik zna ja
                    sim_out << "seated with someone at table " << table_list[i].getID() << endl;
                    is_seated = 1;
                    break;
                }
            }
        }

        if(!is_seated){
            client_group_list.pop_back();
            sim_out << "no free space, left the facility" << endl;
        }
        else{
            group_ptr->set_status(1);
            awaiting.push(group_ptr);
        }

    }
}


void Simulation::manage_awaiting(){
    int i = 0;
    while(!awaiting.empty()){
        while(i<num_of_waiters){
            if(waiter_list[i].is_free()){
                int waiter_status = awaiting.front()->get_status();
                int waiter_turns = get_turns(waiter_status, awaiting.front()->get_num_of_clients());
                waiter_list[i].change_status(waiter_status, waiter_turns);
                waiter_list[i].assign_client(awaiting.front());   //waiter is supposed to be smarter, he changes the client's status when the time comes.

                sim_out << "waiter " << waiter_list[i].get_id() << " assigned for " << waiter_turns;
                sim_out << " min, in service of group " << awaiting.front()->get_id() << endl;

                awaiting.pop();

                i++;
                break;
            }
            i++;
        }
        if(i>=num_of_waiters) break;
    }
}


void Simulation::iterate_waiters(){
    for(int i=0; i<num_of_waiters; i++){
        if(waiter_list[i].next_turn()){
            manage_waiter(&waiter_list[i]);
        }
    }
}


void Simulation::iterate_clients(){
    for(auto it = client_group_list.begin(); it != client_group_list.end(); ++it){
        if(it->next_turn()){
            manage_client(&*it);
        }
    }
}


void Simulation::manage_client(ClientGroup* group_ptr){
    //client's wait is over

    int status = group_ptr->get_status();
    switch(status){
        case 2:
            sim_out << "group nr " << group_ptr->get_id() << " ready to order" << endl;
            awaiting.push(group_ptr);
            break;
        case 3:
            sim_out << "food cooked for group nr " << group_ptr->get_id() << endl;
            awaiting.push(group_ptr);
            break;
        case 4:
            sim_out << "group nr " << group_ptr->get_id() << " finished eating" << endl;
            awaiting.push(group_ptr);
            break;
        default:
            throw runtime_error("Unexpected behavior in client service flow (client)");
            break;
    }
}


void Simulation::manage_waiter(Waiter* waiter_ptr){
    //on job COMPLETION
    ClientGroup* group_ptr = waiter_ptr->get_served_client();
    switch(waiter_ptr->get_status()){
        case 1:
            //give card and call client do start his next action
            sim_out << "card given to group nr " << group_ptr->get_id() << " by waiter nr " << waiter_ptr->get_id() << endl;
            choose_order(group_ptr);
            break;
        case 2:
            //accept order, make bill and call client do start his next action
            sim_out << "order of group nr " << group_ptr->get_id() << " accepted by waiter nr " << waiter_ptr->get_id() << ", ordered: " << endl;
            accept_order(group_ptr, waiter_ptr); //here order contents are made, bill is made and cooking is initiated
            break;
        case 3:
            //give the food and call client do start his next action
            sim_out << "food given to group nr " << group_ptr->get_id() << " by waiter nr " << waiter_ptr->get_id() << endl;
            consume(group_ptr); //clients eat
            break;
        case 4:
            //money and call client do start his next action
            payment(group_ptr, waiter_ptr); //payment process and clients leave
            break;
        default:
            throw runtime_error("Unexpected behavior in client service flow (waiter)");
            break;
    }
    waiter_ptr->remove_client();
}


int Simulation::get_turns(int status, int num_of_clients){
    exponential_distribution<double> dis(1.0);
    switch(status){
        case 1:
            //give card
            dis = exponential_distribution<double>(1.0);
            return(int(dis(gn)) + 1);
            break;
        case 2:
            //accept order
            dis = exponential_distribution<double>(0.5);
            return(int(dis(gn)) + 1);
            break;
        case 3:
            //give_food
            dis = exponential_distribution<double>(0.9);
            return( int((dis(gn)+0.5)*num_of_clients) + 1 );
            break;
        case 4:
            //payment
            dis = exponential_distribution<double>(0.5);
            return(int(dis(gn)) + 2);
            break;
        default:
            throw runtime_error("action order error");
            break;
    }
}


void Simulation::choose_order(ClientGroup* group_ptr){
    exponential_distribution<double> dis(1.0/5.0);
    group_ptr->set_busy_turns(int(dis(gn)) + 2);
    group_ptr->set_status(2);
}


void Simulation::accept_order(ClientGroup* group_ptr, Waiter* waiter_ptr){
    //bill n' stuff, menu, initiate cooking

    double cooking_time = generate_order(group_ptr, waiter_ptr);
    waiter_ptr->end_service();
    sim_out << group_ptr->get_bill()->get_simple_bill();

    exponential_distribution<double> dis(1.0/3.0);
    group_ptr->set_busy_turns( int(dis(gn)) + int(0.8 * cooking_time) );
    group_ptr->set_status(3);
    group_ptr->set_debt(group_ptr->get_bill()->get_sum());
}


int Simulation::generate_order(ClientGroup* group_ptr, Waiter* waiter_ptr){
    int num_of_clients = group_ptr->get_num_of_clients();
    for(int i=0; i<num_of_clients; i++){
        uniform_real_distribution<double> dis(0.0, 1.0);
        double p = dis(gn);
        bool is_drink = 0;
        if(p < 0.7){
            uniform_int_distribution<int> dis1(0, pizza_list.size()-1);
            Pizza* selected_pizza_p = &pizza_list[dis1(gn)];
            waiter_ptr->take_position_of_order(selected_pizza_p, 1);
        }
        else if(p < 0.9){
            uniform_int_distribution<int> dis2(0, snack_list.size()-1);
            Snack* selected_snack_p = &snack_list[dis2(gn)];
            waiter_ptr->take_position_of_order(selected_snack_p, 1);
        }
        else{
            //no main food
            is_drink = 1;
        }

        if(!is_drink){
            //if ordering food, choose if drinking
            bernoulli_distribution dis3(0.6);
            is_drink = dis3(gn);
        }

        if(is_drink){
            uniform_int_distribution<int> dis4(0, drink_list.size()-1);
            Drink* selected_drink_p = &drink_list[dis4(gn)];
            waiter_ptr->take_position_of_order(selected_drink_p, 1);
        }
    }
    Bill* bill_ptr = waiter_ptr->make_bill(0, sim_time);
    int cooking_time = bill_ptr->get_time_prepare();
    group_ptr->assign_bill(bill_ptr);

    return cooking_time;
}


void Simulation::consume(ClientGroup* group_ptr){
    exponential_distribution<double> dis(1.0/10.0);
    group_ptr->set_busy_turns(int(dis(gn)) + 10);
    group_ptr->set_status(4);
}


void Simulation::payment(ClientGroup* group_ptr, Waiter* waiter_ptr){
    //money
    sim_out << "group nr " << group_ptr->get_id() << " paid ";
    int paid = group_ptr->get_debt();
    sim_out << int(paid/100) << ".";
    sim_out.width(2);
    sim_out.fill('0');
    sim_out << paid%100;
    sim_out << " PLN to waiter nr " << waiter_ptr->get_id();
    if(group_ptr->get_age() < 60){
        bernoulli_distribution dis(0.8);
        if(dis(gn)){
            paid += 1000;
            sim_out << ", gave 10 PLN tip for fast service";
        }
    }
    else if(group_ptr->get_age() > 120)
        sim_out << ", complained for slow service";
    sim_out << " and left. " << endl;

    funds += paid;

    group_ptr->pay_debt(paid);

    sim_out << "bill: " << endl << endl;
    sim_out << group_ptr->get_bill()->get_printed_bill();
    delete (group_ptr->get_bill());

    //removing group from the list, freeing table
    remove_from_tables(group_ptr);
    remove_group(group_ptr);
}


void Simulation::remove_from_tables (ClientGroup* group_ptr){
    for(auto it = table_list.begin(); it != table_list.end(); it++){
        if(it->removeClientGroup(group_ptr)) return;
    }
    throw runtime_error("group not found at any table!");
}


void Simulation::remove_group (ClientGroup* group_ptr){
    client_group_list.remove(*group_ptr);
}


void Simulation::print_all(){
    cout << endl << string(25, '-') << endl << "CURRENT SIMULATION STATE:" << endl << endl;
    cout << "T = " << sim_time << endl << endl;

    cout << "Tables: " << endl;
    for(auto it = table_list.begin(); it != table_list.end(); it++){
        cout << *it << endl;
    }

    cout << endl << "Waiters: " << endl;
    for(auto it = waiter_list.begin(); it != waiter_list.end(); it++){
        cout << *it << endl;
    }

    if(!client_group_list.empty()){
        cout << endl << "Clients: " << endl;
        for(auto it = client_group_list.begin(); it != client_group_list.end(); it++){
            cout << *it << endl;
        }
    }
    else
    {
        cout << endl << "No clients now." << endl;
    }

    cout << endl << string(25, '-') << endl;
}


void Simulation::print_menu(){
    cout << endl << string(5, '-') << endl << "MENU:" << endl << endl;

    cout << "Pizzas: " << endl;
    for(auto it = pizza_list.begin(); it != pizza_list.end(); it++){
        cout << *it << endl;
    }

    cout << endl << "Snacks: " << endl;
    for(auto it = snack_list.begin(); it != snack_list.end(); it++){
        cout << *it << endl;
    }

    cout << endl << "Drinks: " << endl;
    for(auto it = drink_list.begin(); it != drink_list.end(); it++){
        cout << *it << endl;
    }

    cout << endl << string(5, '-') << endl;
}


void Simulation::pause_flow(){
    if(!skipping_on){
        char a = cin.get();
        if(a == 's' && cin.peek() == '\n'){
            //skip to end
            skipping_on = 1;
        }
        else if(a == 'n' && cin.peek() == '\n'){
            //print simulation state
            print_all();
        }
        else if(a == 'm' && cin.peek() == '\n'){
            //print menu
            print_menu();
        }
        else if(a != '\n'){
            cout << "Unknown command, interpreting as \"enter\"" << endl;
        }

        cin.unget();
        cin.clear();
        cin.ignore(1000, '\n');
    }
}


void Simulation::turn_action(){
    spawn_clients(); //sprawdzamy czy nowy klient nie przyszedl i usadzamy go
    iterate_waiters(); //kelnerzy ktorzy ukoinczyli wykonywanie akcj iwykonuja dane akcje
    iterate_clients(); //klienci ktorzy ukoinczyli czynnosc sa gotowi do wejscia na kolejke i rozpoczecia kolejnej czynnosci
    manage_awaiting(); //przyporzadkowanie kelnerow osobom w kolejce
    if(!sim_out.str().empty()){
        cout << endl << "T = " << sim_time << endl;
        total_out << endl << "T = " << sim_time << endl;
        out_stream();
        pause_flow();
    }
}


void Simulation::end_action(){
    sim_out << endl << endl << string(34, '-') << endl << "Planned sim time passed, T = " << sim_time << endl;
    sim_out << "Profit: " << int(funds/100) << ".";
    sim_out.width(2);
    sim_out.fill('0');
    sim_out << funds%100;
    sim_out << " PLN" << endl;
    sim_out << string(34, '-') << endl;
    out_stream();
}


void Simulation::out_stream(){
    cout << sim_out.str();
    total_out << sim_out.str();
    sim_out.clear();
    sim_out.str("");
}


void Simulation::main_loop(){
    sim_out << string(31, '-') << endl;
    sim_out << "WELCOME TO RESTAURANT SIMULATOR" << endl;
    sim_out << string(31, '-') << endl;
    sim_out << "Planned time: " << sim_whole_time << " min" << endl;
    sim_out << "Start time: " << sim_time << endl;
    sim_out << "Number of waiters: " << num_of_waiters << endl;
    sim_out << "Expected num of clients per hour: " << client_density << endl;
    out_stream();
    cout << "Press \"enter\" after each message to display next massage," << endl;
    cout << "or \"s\" to display everything ahead instantly," << endl;
    cout << "or \"n\" to print current state of the whole simulation," << endl;
    cout << "or \"m\" to print menu." << endl;
    pause_flow();

    while(sim_time.get_ticks() < sim_whole_time){
        turn_action();
        sim_time.tick();
    }

    end_action();
    Reader reader;
    reader.writeLog(total_out.str());
}
