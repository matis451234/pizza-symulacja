#include"products.h"

using namespace std;

Product::Product(string name_input, int calories_input, int price_input, int prepareTime_input)
{
    name = name_input;
    calories = calories_input;
    price = price_input;
    prepareTime = prepareTime_input;
}

Product::Product() {}

Product::Product(const Product &obj)
{
    name = obj.name;
    calories = obj.calories;
    price = obj.price;
    prepareTime = obj.prepareTime;
}

void Product::setName(string new_name)
{
    name = new_name;
}

string Product::getName()
{
    return name;
}

void Product::setPrepareTime(int new_time)
{
    prepareTime = new_time;
}

int Product::getPrepareTime()
{
    return prepareTime;
}

void Product::setCalories(int new_calories)
{
    calories = new_calories;
}

int Product::getCalories()
{
    return calories;
}

void Product::setPrice(int new_price)
{
    price = new_price;
}

int Product::getPrice()
{
    return price;
}

ostream& operator<<(ostream& os, const Product& obj)
{
    double price_PLN = obj.price;
    os << endl << "Product details:" << endl;
    os << "Name: " << obj.name << endl;
    os << "Calories: " << obj.calories << " kcal" << endl;
    os << "Price: " << price_PLN/100 << " PLN" << endl;
    os << endl;
    return os;
}

Product Product::operator=(const Product &obj)
{
    if (this !=  &obj)
    {
        name = *&obj.name;
        calories = *&obj.calories;
        price = *&obj.price;
    }
    return *this;
}

bool Product::operator==(const Product& obj)
{
    if (name != obj.name)
        return 0;
    else if (calories != obj.calories)
        return 0;
    else if (price != obj.price)
        return 0;
    else
        return 1;
}

bool Product::operator!=(const Product& obj)
{
    if (*this == obj)
        return 0;
    else
        return 1;
}

Pizza::Pizza(string name_input, int calories_input, int price_input, int prepareTime_input, string dough_input, string category_input, vector<string> ingredients_input): Product(name_input, calories_input, price_input, prepareTime_input)
{
    dough = dough_input;
    category = category_input;
    ingredients = ingredients_input;
}

Pizza::Pizza()
{

}

Pizza::Pizza(const Pizza &pizza_obj)
{
    name = pizza_obj.name;
    calories = pizza_obj.calories;
    price = pizza_obj.price;
    prepareTime = pizza_obj.prepareTime;
    dough = pizza_obj.dough;
    category = pizza_obj.category;
    ingredients = pizza_obj.ingredients;
}

void Pizza::setDough(string new_dough)
{
    dough = new_dough;
}

string Pizza::getDough()
{
    return dough;
}

void Pizza::setCategory(string new_category)
{
    category = new_category;
}

string Pizza::getCategory()
{
    return category;
}

void Pizza::setIngredients(vector<string> new_ingredients)
{
    ingredients = new_ingredients;
}

vector<string> Pizza::getIngredients()
{
    return ingredients;
}

string Pizza::getIngredient_as_string()
{
    string ingredients_string = "";
    if (ingredients.empty())
        return ingredients_string;
    else
    {
        for (auto i = ingredients.begin(); i != ingredients.end(); ++i)
            ingredients_string += (*i + " , ");
        for (int i= 0; i < 3;i++)
            ingredients_string.pop_back();
    }
    return ingredients_string;
}

void Pizza::modifyIngredient(int position, string new_ingredient)
{
    ingredients.at(position) = new_ingredient;
}

void Pizza::removeIngredient(int position)
{
    ingredients.erase(ingredients.begin() + position);
}

void Pizza::insertIngredient(int position, string new_ingredient)
{
    ingredients.insert(ingredients.begin() + position, new_ingredient);
}

void Pizza::addIngredient(string new_ingredient)
{
    ingredients.push_back(new_ingredient);
}

ostream& operator<<(ostream& os, const Pizza& pizza_obj)
{
    double price_PLN = pizza_obj.price;
    os << "Name: " << pizza_obj.name;
    os << " Calories: " << pizza_obj.calories << " kcal";
    os << " Dough: " << pizza_obj.dough;
    os << " Category: " << pizza_obj.category;
    os << " Price: " << price_PLN/100 << " PLN";
    os << " Ingredients: ";
    for (auto i = pizza_obj.ingredients.begin(); i != pizza_obj.ingredients.end(); ++i)
        os << *i << " ";
    return os;
}

Pizza Pizza::operator=(const Pizza &pizza_obj)
{
    if (this !=  &pizza_obj)
    {
        name = *&pizza_obj.name;
        calories = *&pizza_obj.calories;
        dough = *&pizza_obj.dough;
        category = *&pizza_obj.category;
        price = *&pizza_obj.price;
        ingredients = *&pizza_obj.ingredients;
    }
    return *this;
}

bool Pizza::operator==(const Pizza& pizza_obj)
{
    if (name != pizza_obj.name)
        return 0;
    else if (calories != pizza_obj.calories)
        return 0;
    else if (dough != pizza_obj.dough)
        return 0;
    else if (category != pizza_obj.category)
        return 0;
    else if (price != pizza_obj.price)
        return 0;
    else if (ingredients != pizza_obj.ingredients)
        return 0;
    else
        return 1;
}

bool Pizza::operator!=(const Pizza& pizza_obj)
{
    if (*this == pizza_obj)
        return 0;
    else
        return 1;
}

Drink::Drink(string name_input, int calories_input, int price_input, int prepareTime_input, int size_input):Product(name_input, calories_input, price_input, prepareTime_input)
{
    size = size_input;
}

Drink::Drink()
{

}

Drink::Drink(const Drink &obj)
{
    name = obj.name;
    calories = obj.calories;
    price = obj.price;
    prepareTime = obj.prepareTime;
    size = obj.size;
}

void Drink::setSize(int new_size)
{
    size = new_size;
}

int Drink::getSize()
{
    return size;
}

ostream& operator<<(ostream& os, const Drink& obj)
{
    double price_PLN = obj.price;
    os << "Name: " << obj.name;
    os << " Calories: " << obj.calories << " kcal";
    os << " Price: " << price_PLN/100 << " PLN";
    os << " Size: " << obj.size << " ml";
    return os;
}

Drink Drink::operator=(const Drink& obj)
{
    if (this !=  &obj)
    {
        name = *&obj.name;
        calories = *&obj.calories;
        price = *&obj.price;
        size = *&obj.size;
    }
    return *this;
}

bool Drink::operator==(const Drink& obj)
{
    if (name != obj.name)
        return 0;
    else if (calories != obj.calories)
        return 0;
    else if (price != obj.price)
        return 0;
    else if (size != obj.size)
        return 0;
    else
        return 1;
}

bool Drink::operator!=(const Drink& obj)
{
    if (*this == obj)
        return 0;
    else
        return 1;
}

Snack::Snack(string name_input, int calories_input, int price_input, int prepareTime_input, string dish_type_input):Product(name_input, calories_input, price_input, prepareTime_input)
{
    dish_type = dish_type_input;
}

Snack::Snack()
{

}

Snack::Snack(const Snack &obj)
{
    name = obj.name;
    calories = obj.calories;
    price = obj.price;
    prepareTime = obj.prepareTime;
    dish_type = obj.dish_type;
}

void Snack::setType(string new_type)
{
    dish_type = new_type;
}

string Snack::getType()
{
    return dish_type;
}

ostream& operator<<(ostream& os, const Snack& obj)
{
    double price_PLN = obj.price;
    os << "Name: " << obj.name;
    os << " Calories: " << obj.calories << " kcal";
    os << " Price: " << price_PLN/100 << " PLN";
    os << " Dish type: " << obj.dish_type;
    return os;
}

Snack Snack::operator=(const Snack& obj)
{
    if (this !=  &obj)
    {
        name = *&obj.name;
        calories = *&obj.calories;
        price = *&obj.price;
        dish_type = *&obj.dish_type;
    }
    return *this;
}

bool Snack::operator==(const Snack& obj)
{
    if (name != obj.name)
        return 0;
    else if (calories != obj.calories)
        return 0;
    else if (price != obj.price)
        return 0;
    else if (dish_type != obj.dish_type)
        return 0;
    else
        return 1;
}

bool Snack::operator!=(const Snack& obj)
{
    if (*this == obj)
        return 0;
    else
        return 1;
}