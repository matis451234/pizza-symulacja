#include <iostream>
#include <string>

using namespace std;

#ifndef EXTRAS_H
#define EXTRAS_H

class Clock
{
    private:
        int minutes;
        int hours;
        int ticks;
    public:
        Clock(int startHour, int startMinute); //zegarek przyjmuje godzine otwartcia restauracji
        Clock();
        void tick(); // minuta + 1
        int get_ticks();
        Clock& operator=(const Clock&);
        friend ostream& operator<<(ostream& os, const Clock& obj);
};

#endif