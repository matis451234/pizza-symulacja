#include <vector>
#include <string>
#include "rachunek.h"


#ifndef CLIENT_H
#define CLIENT_H

class Client{
    public:
        Client(std::string = "anonymous");

        bool operator==(const Client&);
        bool operator!=(const Client&);
        friend std::ostream& operator<< (std::ostream&, Client&);

        std::string get_name() const;
        void set_name(std::string);

        int get_id() const;

    private:
        std::string name;
        int id;

        static int id_max;
};
#endif


#ifndef CLIENT_GROUP_INTER_H
#define CLIENT_GROUP_INTER_H
class ClientGroupInter{
    public:
        virtual ~ClientGroupInter(){};

        virtual std::vector <Client> get_client_list() = 0;
        virtual int get_num_of_clients() = 0;
        virtual void set_client_list(std::vector <Client>) = 0;
        virtual bool add_client(Client) = 0;
        virtual bool add_client(std::string) = 0;
        virtual short remove_client(Client) = 0;
        virtual bool change_client_name(Client, std::string) = 0;

        virtual Client get_group_leader() = 0;
        virtual bool set_group_leader(Client) = 0;

        virtual int get_id() = 0;
        virtual int get_age() = 0;

        virtual int get_table() = 0;
        virtual void set_table(int) = 0;

        virtual int get_status() = 0;
        virtual void set_status(int) = 0;

        virtual int get_busy_turns() = 0;
        virtual void set_busy_turns(int) = 0;
        virtual bool next_turn() = 0;

        virtual int get_budget() = 0;
        virtual void set_budget(int) = 0;

        virtual Bill* get_bill() = 0;
        virtual void assign_bill(Bill*) = 0;

        virtual int get_debt() = 0;
        virtual void set_debt(int) = 0;
        virtual void add_debt(int) = 0;
        virtual int pay_debt(int) = 0;

        virtual std::vector <std::string> get_fav_pizzas() = 0;
        virtual void set_fav_pizzas(std::vector <std::string>) = 0;
        virtual bool add_fav_pizza(std::string) = 0;
        virtual bool remove_fav_pizza(std::string) = 0;

        virtual std::string get_fav_drink() = 0;
        virtual void set_fav_drink(std::string) = 0;
};
#endif


#ifndef CLIENT_GROUP_H
#define CLIENT_GROUP_H
class ClientGroup: public ClientGroupInter{
    public:
        //list of clients, main client required;
        //table, preferences and budget can be set later
        ClientGroup(std::vector <Client>, Client, int=0, int=0, int=0, std::vector <std::string> = {}, std::string="");
        ClientGroup(const ClientGroup&);
        ClientGroup& operator=(const ClientGroup&);

        bool operator==(const ClientGroup&);
        bool operator!=(const ClientGroup&);
        friend std::ostream& operator<< (std::ostream&, ClientGroup&);

        std::vector <Client> get_client_list();
        int get_num_of_clients();
        void set_client_list(std::vector <Client>);
        bool add_client(Client);
        bool add_client(std::string);
        short remove_client(Client);
        bool change_client_name(Client, std::string);

        Client get_group_leader();
        bool set_group_leader(Client);

        int get_id();
        int get_age();

        int get_table();
        void set_table(int);

        int get_status();
        void set_status(int);

        int get_busy_turns();
        void set_busy_turns(int);
        bool next_turn();

        int get_budget();
        void set_budget(int);

        Bill* get_bill();
        void assign_bill(Bill*);

        int get_debt();
        void set_debt(int);
        void add_debt(int);
        int pay_debt(int);

        std::vector <std::string> get_fav_pizzas();
        void set_fav_pizzas(std::vector <std::string>);
        bool add_fav_pizza(std::string);
        bool remove_fav_pizza(std::string);

        std::string get_fav_drink();
        void set_fav_drink(std::string);

    private:
        std::vector <Client> client_list;
        int group_leader;
        int budget;
        int debt;
        int table;
        int status;
        int busy_turns;
        int id;
        int age;
        Bill* bill_ptr;
        std::vector <std::string> fav_pizzas;
        std::string fav_drink;

        int is_client(Client);
        int is_client_id(Client);
        int is_pizza(std::string);
        Client get_client_by_id(int);

        static int id_max;
};
#endif