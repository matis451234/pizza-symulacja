#include <iostream>
#include <cassert>
using namespace std;
#ifndef ELEMENT_H
#define ELEMENT_H


template <class T>
class Element
{
public:
    T value;
    Element<T>* next;
    Element(T v)
    {
        value = v;
    }
    friend ostream& operator<<(ostream& out, const Element<T>& element)
    {
        return out << element->value;
    }

};
#endif

#ifndef LISTA_H
#define LISTA_H
template <class T>
class Lista
{
public:
    int counter = 0;
    Element<T>* head = NULL;
    Element<T>* tail = NULL;


    Lista()
    {

    }

    ~Lista()
    {
        for(int i =0; i < counter; i++)
        {
            remove_element(0);
        }
    }

    void add_element(const T& value)
    {

        if (head == NULL)
        {
            head = new Element<T>(value);
            tail = head;
        }
        else
        {
            Element<T>* v = new Element<T>(value);
            tail->next = v;
            tail = v;
        }
        counter++;
    }


    Element<T>* get_element(int i)
    {

        if (i<0 || i >= counter)
        {
            return NULL;
        }
        Element<T>* tmp =head;
        for (int j = 0; j<i; j++)
        {
            tmp = tmp -> next;
        }
        return tmp;
    }


    void remove_element(int i)
    {

        if (i<0 || i >= counter)
        {
            cout<<"Brak takiego indeksu"<<endl;
            return;
        }
        if (i == 0)
        {
            Element<T>* tmp = head->next;
            delete head;
            head = tmp;
        }
        else if (i == counter - 1)
        {
            Element<T>* tmp = get_element(i-1);
            Element<T>* v = tmp->next;
            tmp->next = NULL;
            tail = tmp;
            delete v;
        }
        else
        {
            Element<T>* tmp = get_element(i-1);
            Element<T>* v = tmp->next;
            tmp->next = v->next;
            delete v;
        }
        counter--;
        if (counter == 0) {
            head = NULL;
            tail = NULL;
        }
    }




    Element<T>* get_next_element(int i)
    {

        if (i<0 || i >= counter - 1)
        {
            return NULL;
        }
        Element<T>* tmp =head;
        for (int j = 0; j<i+1; j++)
        {
            tmp = tmp -> next;
        }
        return tmp;
    }


    bool list_is_empty()
    {
        if(counter == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    int get_size()
    {
        return counter;
    }

    void print_list()
    {
        for(int i = 0; i < counter; i++)
        {
            Element<T>* a1 = get_element(i);
            T v1 = a1->value;
            cout<<v1<<endl;
        }
        cout<<endl;
    }
};



#endif





