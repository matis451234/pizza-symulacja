#include <iostream>
#include "simulation.h"
#include "filereader.h"

using namespace std;

int main(){
    Reader filereader;
    vector<Pizza> pizzas = filereader.readPizzas("pizza_menu.csv");
    vector<Drink> drinks = filereader.readDrinks("drink_menu.csv");
    vector<Snack> snacks = filereader.readSnacks("snack_menu.csv");
    vector<Table> tables = filereader.readTables("tables.csv");
    vector<int> config = filereader.readConfig("config.csv");
    filereader.clearLog();
    Simulation sim(config[0], config[3], config[4], tables, pizzas, drinks, snacks, config[1], config[2]);
    sim.main_loop();
    return 0;
}