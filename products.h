#include <iostream>
#include <string>
#include <vector>

using namespace std;

#ifndef PRODUCT_H
#define PRODUCT_H

class Product
{
protected:
    string name;
    int calories;
    int price;
    int prepareTime;
public:
    Product(string name_input, int calories_input, int price_input, int prepareTime_input);
    Product();
    Product(const Product &pizza_obj);
    void setName(string);
    string getName();
    void setCalories(int);
    int getCalories();
    void setPrice(int);
    int getPrice();
    void setPrepareTime(int);
    int getPrepareTime();
    friend ostream& operator<<(ostream& os, const Product& pizza_obj);
    Product operator=(const Product& pizza_obj);
    bool operator==(const Product& pizza_obj);
    bool operator!=(const Product& pizza_obj);
};

#endif


#ifndef PIZZA_H
#define PIZZA_H

class Pizza: public Product
{
private:
    string dough;
    string category;
    vector<string> ingredients;
public:
    Pizza(string name_input, int calories_input, int price_input, int prepareTime, string dough_input, string category_input,vector<string> ingredients_input);
    Pizza();
    Pizza(const Pizza &pizza_obj);
    void setDough(string);
    string getDough();
    void setCategory(string);
    string getCategory();
    void setIngredients(vector<string>);
    vector<string> getIngredients();
    string getIngredient_as_string();
    void modifyIngredient(int, string);
    void removeIngredient(int);
    void insertIngredient(int, string);
    void addIngredient(string);
    friend ostream& operator<<(ostream& os, const Pizza& pizza_obj);
    Pizza operator=(const Pizza& pizza_obj);
    bool operator==(const Pizza& pizza_obj);
    bool operator!=(const Pizza& pizza_obj);
};

#endif


#ifndef SNACK_H
#define SNACK_H

class Snack: public Product
{
private:
    string dish_type;
public:
    Snack(string name_input, int calories_input, int price_input, int prepareTime_input, string dish_type_input);
    Snack();
    Snack(const Snack &obj);
    void setType(string);
    string getType();
    friend ostream& operator<<(ostream& os, const Snack& obj);
    Snack operator=(const Snack& obj);
    bool operator==(const Snack& obj);
    bool operator!=(const Snack& bj);
};

#endif


#ifndef DRINK_H
#define DRINK_H

class Drink: public Product
{
private:
    int size;
public:
    Drink(string name_input, int calories_input, int price_input, int prepareTime_input, int size_input);
    Drink();
    Drink(const Drink &obj);
    void setSize(int);
    int getSize();
    friend ostream& operator<<(ostream& os, const Drink& obj);
    Drink operator=(const Drink& obj);
    bool operator==(const Drink& obj);
    bool operator!=(const Drink& bj);
};

#endif