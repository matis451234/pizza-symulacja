#include "waiter.h"
#include "lista.h"
#include "rachunek.h"
#include "products.h"
#include <vector>
#include <iostream>
using namespace std;



void Waiter::change_status(int s, int b)
{
    status = s;
    busy_turns = b;
}


bool Waiter::is_free()
{
    if (busy_turns == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

int Waiter::get_id(){
    return waiter_id;
}

void Waiter::take_position_of_order(Product* p, int amount)
{
    bool position_not_added;
    position_not_added = true;
    for(int i = 0; i < list_of_things.get_size(); i++)
    {
        if (p == list_of_things.get_element(i)->value)
        {
            list_of_counts.get_element(i)->value += amount;
            position_not_added = false;
        }
    }
    if(position_not_added)
    {
    list_of_things.add_element(p);
    list_of_counts.add_element(amount);
    }
}

Bill* Waiter::make_bill(int discount, Clock time)
{
    Bill* b = new Bill(waiter_id);
    for(int i = 0; i < list_of_things.get_size(); i++)
    {
        b->add_position(list_of_things.get_element(i)->value->getName(),
                        list_of_counts.get_element(i)->value,
                        list_of_things.get_element(i)->value->getPrice(),
                        list_of_things.get_element(i)->value->getPrepareTime());
    }
    b->add_discount(discount);
    b->print_bill(time);
    return b;
}

void Waiter::end_service()
{

    for(int i = 0; i < list_of_things.get_size(); i++)
    {
        list_of_things.remove_element(0);
        list_of_counts.remove_element(0);
        client_ptr = nullptr;
    }

}

void Waiter::remove_client()
{
    client_ptr = NULL;
}


void Waiter::assign_client(ClientGroup* group_ptr)
{
    client_ptr = group_ptr;
}

ClientGroup* Waiter::get_served_client()
{
    return client_ptr;
}

int Waiter::get_status()
{
    return status;
}

bool Waiter::next_turn()
{
    bool marker_next_turn = false;
    if (busy_turns > 0)
    {
       busy_turns--;
       if (busy_turns == 0)
       {
           marker_next_turn = true;
       }
    }
    return marker_next_turn;
}
