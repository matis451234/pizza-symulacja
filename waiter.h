#ifndef WAITER_H
#define WAITER_H
#include <vector>
#include <iostream>
#include "lista.h"
#include "rachunek.h"
#include "products.h"
#include "pizza_client.h"
using namespace std;

class Waiter_Inter
{
public:
    virtual ~Waiter_Inter(){};
    virtual void change_status(int status, int busy_turns) = 0;
    virtual Bill* make_bill(int discount, Clock time) = 0;
    virtual bool is_free() = 0;
    virtual void take_position_of_order(Product*, int) = 0;
    virtual void end_service() = 0;
    virtual void remove_client() = 0;
    virtual void assign_client(ClientGroup* group_ptr) = 0;
    virtual ClientGroup* get_served_client() = 0;
    virtual int get_status() = 0;
    virtual int get_id() = 0;
    virtual bool next_turn() = 0;


};

class Waiter: public Waiter_Inter
{

    int waiter_id;
    int status;
    int busy_turns;
    int amount;
    Lista<Product*> list_of_things;
    Lista<int> list_of_counts;
    ClientGroup* client_ptr;
    bool marker_next_turn;

    public:

    Waiter(int id)
    {
        waiter_id = id;
        status = 0;
        busy_turns = 0;
        client_ptr = nullptr;
    }
    ~Waiter()
    {

    }
    void change_status(int status, int busy_turns);
    Bill* make_bill(int discount, Clock time);
    bool is_free();
    void take_position_of_order(Product*, int);
    void end_service();
    void remove_client();
    void assign_client(ClientGroup* group_ptr);
    ClientGroup* get_served_client();
    int get_status();
    int get_id();
    bool next_turn();
    friend ostream& operator<<(ostream& out, const Waiter& waiter) {
        if(waiter.client_ptr)
        {
            return out <<"Waiter: "<< waiter.waiter_id<< " working for client group: " << waiter.client_ptr->get_id();
        }
        else
        {
            return out <<"Waiter: "<< waiter.waiter_id<< " not busy ";
        }
    }
};

#endif

