#include "table.h"


using namespace std;

Table_Inter::~Table_Inter()
{
    
}

Table::Table(int _ID, int _size)
{
    ID = _ID;
    size = _size;
    empty = _size;
}

Table::Table()
{

}

int Table::getSize()
{
    return size;
}

int Table::getID()
{
    return ID;
}

int Table::emptySeats()
{
    return empty;
}

bool Table::is_empty()
{
    if (clients.empty())
        return 1;
    else
        return 0;
}

bool Table::canSitDown(ClientGroup *possible_client)
{
    int num_of_clients = possible_client->get_num_of_clients();
    if (num_of_clients > empty)
        return 0;
    else
    {
        switch (empty - num_of_clients)
        {
        case 0:
            if (rand() % 99 < 25)
                return 1;
            else
                return 0;
        case 1:
            if (rand() % 99 < 50)
                return 1;
            else
                return 0;
        case 2:
            if (rand() % 99 < 75)
                return 1;
            else
                return 0;
        default: return 1;
        }
    }
}

bool Table::can_sit_empty(ClientGroup *possible_client)
{
    if (possible_client->get_num_of_clients() <= empty && is_empty())
        return 1;
    else
        return 0;
}

void Table::addClientGroup(ClientGroup* new_client_group)
{
    clients.push_back(new_client_group);
    empty = empty - new_client_group-> get_num_of_clients();
}

bool Table::removeClientGroup(ClientGroup *old_client)
{
    for (auto i = clients.begin(); i != clients.end(); ++i)
        if (*i == old_client)
        {
            empty = empty + old_client->get_num_of_clients();
            clients.erase(i);
            return 1;
        }
    return 0;
}

ostream& operator<<(ostream& os, const Table& obj)
{
    os << "Table " << obj.ID << ", " << obj.empty << " empty seats";
    if(!obj.clients.empty()){
        os << ", clients: ";
        for(auto const& value: obj.clients)
        {
            os << "group " <<value->get_id() << " ";
        }
    }
    return os;
}