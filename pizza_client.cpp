#include "pizza_client.h"

#include <iostream>
#include <vector>
#include <string>

using namespace std;



//CLASS CLIENT

Client::Client(string name){
    this->id = Client::id_max;
    Client::id_max ++;
    this->name = name;
}


string Client::get_name() const{
    return(this->name);
}


void Client::set_name(string name){
    this->name = name;
}


int Client::get_id() const{
    return(this->id);
}


int Client::id_max;


bool Client::operator==(const Client& comp){
    return(get_id() == comp.get_id());
}


bool Client::operator!=(const Client& comp){
    return(this->get_id() != comp.get_id());
}

ostream& operator<<(ostream& os, Client& cl){
    os << cl.get_name() << endl;
    return os;
}


//CLASS CLIENT GROUP

ClientGroup::ClientGroup(vector <Client> client_list, Client group_leader, int budget,
                int debt, int table, vector <string> fav_pizzas, string fav_drink){
    this->client_list = client_list;
    this->group_leader = is_client_id(group_leader);
    this->budget = budget;
    this->debt = debt;
    this->table = table;
    this->fav_pizzas = fav_pizzas;
    this->fav_drink = fav_drink;
    this->age = 0;
}


ClientGroup::ClientGroup(const ClientGroup& comp){
    this->id = ClientGroup::id_max;
    ClientGroup::id_max ++;
    this->client_list = comp.client_list;
    this->group_leader = comp.group_leader;
    this->table = comp.table;
    this->budget = comp.budget;
    this->debt = comp.debt;
    this->fav_pizzas = comp.fav_pizzas;
    this->fav_drink = comp.fav_drink;
    this->age = 0;
}


// ClientGroup::~ClientGroup(){
//     delete[] client_list;
// }


ClientGroup& ClientGroup::operator=(const ClientGroup& comp){
    if(this != &comp){
        this->id = ClientGroup::id_max;
        ClientGroup::id_max ++;
        this->client_list = comp.client_list;
        this->group_leader = comp.group_leader;
        this->table = comp.table;
        this->budget = comp.budget;
        this->debt = comp.debt;
        this->fav_pizzas = comp.fav_pizzas;
        this->fav_drink = comp.fav_drink;
    }
    return *this;
}


bool ClientGroup::operator==(const ClientGroup& comp){
    if(this->client_list.size() == comp.client_list.size()){
        for (unsigned int it = 0; it < this->client_list.size(); it++){
            bool is_avail = 0;
            for (unsigned int jt = 0; jt < this->client_list.size(); jt++){
                if(this->client_list[it] == comp.client_list[jt]){
                    is_avail = 1;
                    break;
                }
            }
            if(is_avail == 0){
                return 0;
            }
        }
        if(this->group_leader != comp.group_leader) return 0;
        if(this->table != comp.table) return 0;
        if(this->budget != comp.budget) return 0;
        if(this->debt != comp.debt) return 0;
        if(this->fav_pizzas != comp.fav_pizzas) return 0;
        if(this->fav_drink != comp.fav_drink) return 0;
        return 1;
    }
    else
        return 0;
}


bool ClientGroup::operator!=(const ClientGroup& comp){
    return !(*this == comp);
}


int ClientGroup::get_id(){
    return(this->id);
}


int ClientGroup::get_age(){
    return(this->age);
}


int ClientGroup::id_max = 0;


vector <Client> ClientGroup::get_client_list(){
    return(this->client_list);
}


void ClientGroup::set_client_list(vector <Client> client_list){
    this->client_list = client_list;
}


int ClientGroup::is_client(Client searched_client){
    for (unsigned int it = 0; it < this->client_list.size(); it++){
        if(this->client_list[it] == searched_client){
            return it;
        }
    }
    return -1;
}


bool ClientGroup::add_client(Client new_client){
    if(this->is_client(new_client) == -1){
        this->client_list.push_back(new_client);
        return 1;
    }
    return 0;
}

bool ClientGroup::add_client(string new_client_name){
    Client new_client(new_client_name);
    this->client_list.push_back(new_client);
    return 1;
}

short ClientGroup::remove_client(Client new_client){
    int client_it = this->is_client(new_client);
    if(client_it != -1){
        Client leader = this->get_group_leader();
        if(this->client_list[client_it] == leader)
            return -1;
        this->client_list.erase(this->client_list.begin() + client_it);
        return 1;
    }
    return 0;
}


bool ClientGroup::change_client_name(Client new_client, string new_name){
    int client_it = this->is_client(new_client);
    if(client_it != -1){
        this->client_list[client_it].set_name(new_name);
        return 1;
    }
    return 0;
}


int ClientGroup::is_client_id(Client searched_client){
    for (unsigned int it = 0; it < this->client_list.size(); it++){
        if(this->client_list[it] == searched_client){
            return this->client_list[it].get_id();
        }
    }
    return -1;
}


Client ClientGroup::get_client_by_id(int searched_client_id){
    for (unsigned int it = 0; it < this->client_list.size(); it++){
        if(this->client_list[it].get_id() == searched_client_id){
            return this->client_list[it];
        }
    }
    return Client();
}


Client ClientGroup::get_group_leader(){
    return(get_client_by_id(this->group_leader));
}


bool ClientGroup::set_group_leader(Client new_client){
    int client_id = this->is_client_id(new_client);
    if(client_id != -1){
        this->group_leader = client_id;
        return 1;
    }
    return 0;
}


int ClientGroup::get_table(){
    return(this->table);
}


void ClientGroup::set_table(int table){
   this->table = table;
}


int ClientGroup::get_status(){
    return(this->status);
}


void ClientGroup::set_status(int status){
   this->status = status;
}


int ClientGroup::get_busy_turns(){
    return(this->busy_turns);
}


void ClientGroup::set_busy_turns(int busy_turns){
   this->busy_turns = busy_turns;
}


bool ClientGroup::next_turn(){
   age ++;
   if(busy_turns > 1){
       busy_turns--;
   }
   else if(busy_turns==1){
       busy_turns=0;
       return 1;
   }
   return 0;
}


int ClientGroup::get_budget(){
    return(this->budget);
}


void ClientGroup::set_budget(int budget){
   this->budget = budget;
}


Bill* ClientGroup::get_bill(){
    return(this->bill_ptr);
}


void ClientGroup::assign_bill(Bill* bill){
   this->bill_ptr = bill;
}


int ClientGroup::get_debt(){
    return(this->debt);
}


void ClientGroup::set_debt(int debt){
   this->debt = debt;
}


void ClientGroup::add_debt(int debt){
   this->debt += debt;
}


int ClientGroup::pay_debt(int debt){
    this->debt -= debt;
    if(this->debt < 0){
        int exchange = -this->debt;
        this->debt = 0;
        return exchange;
    }
    else
        return 0;
}



int ClientGroup::get_num_of_clients(){
    return(this->client_list.size());
}


vector <string> ClientGroup::get_fav_pizzas(){
    return(this->fav_pizzas);
}


void ClientGroup::set_fav_pizzas(vector <string> fav_pizzas){
    this->fav_pizzas = fav_pizzas;
}


int ClientGroup::is_pizza(string searched_pizza){
    for (unsigned int it = 0; it < this->fav_pizzas.size(); it++){
        if(this->fav_pizzas[it] == searched_pizza){
            return it;
        }
    }
    return -1;
}


bool ClientGroup::add_fav_pizza(string new_pizza_name){
    int pizza_it = this->is_pizza(new_pizza_name);
    if(pizza_it == -1){
        this->fav_pizzas.push_back(new_pizza_name);
        return 1;
    }
    return 0;
}


bool ClientGroup::remove_fav_pizza(string new_pizza_name){
    int pizza_it = this->is_pizza(new_pizza_name);
    if(pizza_it != -1){
        this->fav_pizzas.erase(this->fav_pizzas.begin() + pizza_it);
        return 1;
    }
    return 0;
}


string ClientGroup::get_fav_drink(){
    return(this->fav_drink);
}


void ClientGroup::set_fav_drink(string fav_drink){
   this->fav_drink = fav_drink;
}


// ostream& operator<<(ostream& os, ClientGroup& cg){
//     os << cg.get_num_of_clients() << " members: " << endl;
//     for (unsigned int it = 0; it < cg.client_list.size(); it++){
//         os << cg.client_list[it].get_name() << endl;
//     }

//     os << "Representative: " << cg.get_group_leader().get_name() << endl;

//     os << "Budget: ";
//     int budget = cg.get_budget();
//     if(budget != 0)
//         os << budget << " gr" << endl;
//     else
//         os << "Not specified" << endl;

//     os << "Debt: ";
//     int debt = cg.get_debt();
//     os << debt << " gr" << endl;

//     os << "Table";
//     int table = cg.get_table();
//     if(table != 0)
//         os << " nr: " << table << endl;
//     else
//         os << ": not specified" << endl;

//     os << "Favourite pizzas:" << endl;
//     if(cg.fav_pizzas.size() != 0)
//         for (unsigned int it = 0; it < cg.fav_pizzas.size(); it++){
//             os << cg.fav_pizzas[it] << endl;
//         }
//     else
//         os << "not specified" << endl;

//     os << "Favourite drink";
//     string fav_drink = cg.get_fav_drink();
//     if(fav_drink != "")
//         os << ": " << fav_drink << endl;
//     else
//         os << ": not specified" << endl;

//     return os;
// }

ostream& operator<<(ostream& os, ClientGroup& cg){
    os << "Group " << cg.get_id() << " of ";
    os << cg.get_num_of_clients() << " members ";

    switch(cg.status){
        case 1:
            os << "is waiting for card.";
            break;
        case 2:
            os << "is choosing their order.";
            break;
        case 3:
            os << "is waiting for their food.";
            break;
        case 4:
            os << "is eating.";
            break;
        default:
            throw runtime_error("Undefined group status!");
            break;
    }

    return os;
}