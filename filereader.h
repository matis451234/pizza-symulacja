#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include "products.h"
#include "table.h"

using namespace std;

#ifndef READER_H
#define READER_H

class Reader
{
    public:
        Reader();
        vector<Pizza> readPizzas(string);
        vector<Snack> readSnacks(string);
        vector<Drink> readDrinks(string);
        vector<Table> readTables(string);
        ifstream openFile(string);
        vector<int> readConfig(string);
        void writeLog(string);
        void clearLog();
};

#endif