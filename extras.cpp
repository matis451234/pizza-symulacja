#include"extras.h"

using namespace std;

Clock::Clock(int startHour, int startMinute)
{
    hours = startHour;
    minutes = startMinute;
    ticks = 0;
}

Clock::Clock() {
    hours = 0;
    minutes = 0;
}

void Clock::tick()
{
    ticks++;
    minutes++;
    if (minutes>=60)
    {
        hours++;
        minutes = minutes - 60;
        if (hours>= 24)
        {
            hours = hours - 24;
        }
    }
}

int Clock::get_ticks() {
    return ticks;
}

Clock& Clock::operator=(const Clock& comp) {
    if(this != &comp){
        this->ticks = comp.ticks;
        this->minutes = comp.minutes;
        this->hours = comp.hours;
    }
    return *this;
}

ostream& operator<<(ostream& os, const Clock& obj)
{
    os.width(2);
    os.fill('0');
    os << obj.hours << ":";
    os.width(2);
    os.fill('0');
    os << obj.minutes;
    return os;
}