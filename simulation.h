#include <vector>
#include <string>
#include <queue>
#include <list>
#include <random>
#include <iostream>
#include <sstream>

#include "waiter.h"
#include "pizza_client.h"
#include "products.h"
#include "table.h"
#include "rachunek.h"
#include "extras.h"
#include "filereader.h"

using namespace std;


#ifndef SIMULATION_H
#define SIMULATION_H
class Simulation{
    public:
        Simulation(int sim_whole_time, int client_density, int num_of_waiters, vector<Table> table_list,
                   vector<Pizza> pizza_list, vector<Drink> drink_list, vector<Snack> snack_list, int=8, int=0);
        //~Simulation();

        void next_turn();
        void turn_action();
        void end_action();
        void main_loop();
        void pause_flow();
        void out_stream();
        void print_all();
        void print_menu();

        void spawn_clients();
        void manage_awaiting();
        void iterate_waiters();
        void iterate_clients();

        void manage_waiter(Waiter*);
        void manage_client(ClientGroup*);
        void choose_order(ClientGroup*);
        void accept_order(ClientGroup*, Waiter*);
        void consume(ClientGroup*);
        void payment(ClientGroup*, Waiter*);
        void remove_from_tables(ClientGroup*);
        void remove_group(ClientGroup*);


    private:
        mt19937 gn;
        Clock sim_time;
        int sim_whole_time;
        int num_of_waiters;
        int funds;
        int num_of_tables; //to be extracted from the file
        int client_density; //avg num of clients per hour
        bool skipping_on;
        stringstream sim_out;
        stringstream total_out;
        vector<Table> table_list;
        list<ClientGroup> client_group_list;
        vector<Waiter> waiter_list;
        vector<Pizza> pizza_list;
        vector<Drink> drink_list;
        vector<Snack> snack_list;
        queue<ClientGroup*> awaiting;

        int get_turns(int, int);

        void generate_waiter_list();
        int generate_order(ClientGroup*, Waiter*);
};
#endif