#include <iostream>
#include "rachunek.h"
#include <ctime>
#include <iomanip>
#include <cmath>
#include <sstream>


Bill::Bill(int i)
{
    seller = to_string(i);
    time_prepare = 0;
}


void Bill::add_position(string n, int c, int p, int t)
{
    list_of_names.push_back(n);
    list_of_count.push_back(c);
    list_of_prices.push_back(p);
    list_of_times.push_back(t);
}


void Bill::print_bill(Clock time)
{
    sum = 0;
    int sum_to_print;
    time_prepare = 0;
    int space_amount;
    int space_amount_position = 0;
    float discount_to_print;
    float price_to_print;
    ss<<"Seller: "<<seller<<"\t"<<time<<endl;
    for(unsigned i=0; i < list_of_names.size(); i++)
    {
        price_to_print = list_of_prices[i];
        price_to_print = price_to_print /100;
        ss<<fixed<< setprecision(2);
        if(i+1<10)
        {
            space_amount_position = 1;
        }
        else if(i+1 >= 10 && i+1 < 100 )
        {
            space_amount_position = 2;
        }
        else
        {
            space_amount_position = 3;
        }

        space_amount = 25 - list_of_names[i].length() - space_amount_position;
        ss<<i+1<<". "<< list_of_names[i];
        for(int j = 0; j < space_amount; j++)
        {
            ss<<" ";
        }
        ss<< list_of_count[i] <<"\t\t price: "<< price_to_print << " each" << endl;
        sum += list_of_prices[i] * list_of_count[i];
        if(list_of_times[i] > time_prepare)
        {
            time_prepare = list_of_times[i];
        }
    }
    if (discount != 0)
    {
        discount_to_print = sum * abs(discount) / 100;
        sum = sum - (sum * abs(discount) / 100);
        discount_to_print = discount_to_print / 100;
        ss<<"Discount: "<<discount<<"% \t\t\t-"<<discount_to_print<<endl;
    }
    sum_to_print = sum;
    sum_to_print = sum_to_print / 100;
    ss<<"Total: "<<sum_to_print<<".00 PLN" <<endl<<endl;
}

void Bill::remove_position(int p)
{
    auto it_n = list_of_names.begin() + p - 1;
    auto it_c = list_of_count.begin() + p - 1;
    auto it_p = list_of_prices.begin() + p - 1;

    list_of_names.erase(it_n);
    list_of_count.erase(it_c);
    list_of_prices.erase(it_p);
}

void Bill::add_discount(int procent)
{
    discount = procent;

}

void Bill::remove_discount()
{
    discount = 0;
}

int Bill::get_sum()
{
    return sum;
}

int Bill::get_time_prepare()
{
    return time_prepare;
}

string Bill::get_printed_bill()
{
    return(ss.str());
}

string Bill::get_simple_bill()
{
    stringstream sss;
    for(unsigned i=0; i < list_of_names.size(); i++)
    {
        sss<<fixed<< setprecision(2);
        int space_amount_position = 0;
        if(i+1<10)
        {
            space_amount_position = 1;
        }
        else if(i+1 >= 10 && i+1 < 100 )
        {
            space_amount_position = 2;
        }
        else
        {
            space_amount_position = 3;
        }
        int space_amount = 25 - list_of_names[i].length() - space_amount_position;
        sss<<i+1<<". "<< list_of_names[i];
        for(int j = 0; j < space_amount; j++)
        {
            sss<<" ";
        }
        sss<< list_of_count[i] <<endl;
    }
    return(sss.str());
}
