#include "filereader.h"

using namespace std;

Reader::Reader()
{

}

ifstream Reader::openFile(string file_name)
{

    ifstream file;
    try
    {
        file.open(file_name);
        if (!file)
        {
            throw string("Unnable to read file: " + file_name);
        }
    }
    catch (string err)
    {
        cout << err;
        exit(0);
    }
    return file;
}

vector<Pizza> Reader::readPizzas(string file_name)
{
    vector<Pizza> Pizza_Menu;
    ifstream file = openFile(file_name);
    string temp_line, temp;
    getline(file, temp_line); // pominiecie 1 linii z nazwami pól
    while (getline(file, temp_line))
    {   
        if (temp_line == "")
            continue;
        vector<string> info;
        vector<string> ingredients;
        int i = 0;
        stringstream ss(temp_line);
        while(getline(ss, temp, ','))
        { 
            if(i < 6) // numer pozycji na której zaczynaja sie skladniki
                info.push_back(temp);
            else
                ingredients.push_back(temp);
            i++;
        }
        try
        {
            Pizza_Menu.push_back(Pizza(info[0], stoi(info[1]),stoi(info[2]),stoi(info[3]),info[4],info[5], ingredients));
        }
        catch (const invalid_argument& err)
        {
            cout << "Error: Invalid argument passed to pizza constructor";
            exit(0);
        }
    }
    file.close();
    try
    {
        if (Pizza_Menu.empty())
            throw (string("Error: Given Pizza menu is empty"));
    }
    catch (string err)
    {
        cout << err;
        exit(0);
    }
    return Pizza_Menu;
}

vector<Snack> Reader::readSnacks(string file_name)
{
    vector<Snack> Snacks_Menu;
    ifstream file = openFile(file_name);
    string temp_line, temp;
    getline(file, temp_line);
    while (getline(file, temp_line))
    {   
        if (temp_line == "")
            continue;
        vector<string> info;
        stringstream ss(temp_line);
        while(getline(ss, temp, ','))
        { 
            info.push_back(temp);
        }
        try
        {
            Snacks_Menu.push_back(Snack(info[0], stoi(info[1]),stoi(info[2]),stoi(info[3]),info[4]));
        }
        catch (const invalid_argument& err)
        {
            cout << "Error: Invalid argument passed to snack constructor";
            exit(0);
        }
    }
    file.close();
    try
    {
        if (Snacks_Menu.empty())
            throw (string("Error: Given Snack menu is empty"));
    }
    catch (string err)
    {
        cout << err;
        exit(0);
    }
    return Snacks_Menu;
}

vector<Drink> Reader::readDrinks(string file_name)
{
    vector<Drink> Drinks_Menu;
    ifstream file = openFile(file_name);
    string temp_line, temp;
    getline(file, temp_line);
    while (getline(file, temp_line))
    {
        if (temp_line == "")
            continue;
        vector<string> info;
        stringstream ss(temp_line);
        while(getline(ss, temp, ','))
        { 
            info.push_back(temp);
        }
        try{
            Drinks_Menu.push_back(Drink(info[0], stoi(info[1]),stoi(info[2]),stoi(info[3]),stoi(info[4])));
        }
        catch (const invalid_argument& err)
        {
            cout << "Error: Invalid argument passed to drink constructor";
            exit(0);
        }
    }
    file.close();
    try
    {
        if (Drinks_Menu.empty())
            throw (string("Error: Given Drink menu is empty"));
    }
    catch (string err)
    {
        cout << err;
        exit(0);
    }
    return Drinks_Menu;
}

vector<Table> Reader::readTables(string file_name)
{
    vector<Table> Tables;
    ifstream file = openFile(file_name);
    string temp_line, temp;
    getline(file, temp_line);
    while (getline(file, temp_line))
    {   
        if (temp_line == "")
            continue;
        vector<string> info;
        stringstream ss(temp_line);
        while(getline(ss, temp, ','))
        { 
            info.push_back(temp);
        }
        try
        {
            Tables.push_back(Table(stoi(info[0]), stoi(info[1])));
        }
        catch (const invalid_argument& err)
        {
            cout << "Error: Invalid argument passed to table constructor";
            exit(0);
        }
    }
    file.close();
    try
    {
        if (Tables.empty())
            throw (string("Error: Given Tables list is empty"));
    }
    catch (string err)
    {
        cout << err;
        exit(0);
    }
    return Tables;
}

vector<int> Reader::readConfig(string file_name)
{
    vector<int> config;
    ifstream file = openFile(file_name);
    string temp_line, temp;
    getline(file, temp_line); // 1 linia z nazwami pol
    getline(file, temp_line); // 2 linia z wartosciami
    stringstream ss(temp_line);
    while(getline(ss, temp, ','))
    {
        try
        {   
            config.push_back(stoi(temp));
        }
        catch (const invalid_argument& err)
        {
            cout << "Error: Invalid argument in config";
            exit(0);
        }
    }
    file.close();
    try
    {
        if (config.size() != 5)
            throw (string("Error: Number of config parameters is diffrent then 5"));
        else if (config[0] <= 0 || config[3] <= 0 || config[4] <= 0)
            throw (string("Error: Non postive argument in whole_time/client_density/waiter_num"));
        else if (config[1] < 0 || config[1] > 23)
            throw (string("Error: Wrong start_hour (negative or over 23)"));
        else if (config[2] < 0 || config[2] > 59)
            throw (string("Error: Wrong start_minute (negative or over 59)"));
    }
    catch (string err)
    {
        cout << err;
        exit(0);
    }
    return config;
}

void Reader::writeLog(string log_msg)
{
    ofstream file;
    file.open("logs.txt", ofstream::app);
    file << log_msg << endl;
    file.close();
}

void Reader::clearLog()
{
    ofstream file;
    file.open("logs.txt", ofstream::out | ofstream::trunc);
    file.close();
}